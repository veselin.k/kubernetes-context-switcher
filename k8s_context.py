#!/usr/bin/env python3

import os
import curses
import logging
import textwrap
import argparse
import npyscreen
from kubeconfig import KubeConfig
from kubernetes import client, config

__author__ = "Veselin Karamanski"


parser = argparse.ArgumentParser(prog='npy_k8s_context.py',
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=textwrap.dedent('''\
                                 List and set kubernetes context
                                 -------------------------------
                                 "SPACE" - expand the namespaces for the selected cluster
                                 "ENTER" - set cluster/namespace
                                 "q" or "ESC" - quit
                                 '''))
args = parser.parse_args()


def get_namespaces(context):
    k8s_client = client.CoreV1Api(config.new_client_from_config(context=context))
    log = logging.getLogger("urllib3")
    log.setLevel(logging.CRITICAL)
    nss = k8s_client.list_namespace(timeout_seconds=3).items
    return [ns.metadata.name for ns in nss]


def get_context_name(cluster):
    return next(i['name'] for i in contexts if i['context']['cluster'] == cluster)


class K8sContextsForm(npyscreen.FormBaseNew):
    """K8sContextsForm"""


    def __init__(self, *args, **kwargs):
        super(K8sContextsForm, self).__init__(*args, **kwargs)
        self._search_string = ""

    def handle_key_stroke(self, char):
        dir(char)
        self._search_string += chr(char)
        print(self._search_string)
        # TO create filter for generating the list.


    def create(self):
        """create"""
        self.tree_wgd = self.add(npyscreen.MLTree)
        self.tree_data_ctx = npyscreen.TreeData(content='Root')
        for ctx in contexts:
            if ctx == active_context:
                ctx_index= contexts.index(active_context)
            contexts.append
            try:
                context = '{}:{}'.format(ctx['context']['cluster'], ctx['context']['namespace'])
            except KeyError:
               context = ctx['context']['cluster']
            self.tree_data_ctx.new_child(content=context, expanded=False)
        self.tree_wgd.values = self.tree_data_ctx
        self.tree_wgd.cursor_line=ctx_index

        handlers = {
            "q": self.exit_func,
            curses.ascii.ESC: self.exit_func,
            curses.ascii.SP: self.expand_namespaces,
            curses.ascii.LF: self.set_context,
            curses.KEY_DC: self.delete_context, # To be done
        }
        self.tree_wgd.add_handlers(handlers)

        complex_handlers = [(curses.ascii.isalnum, self.handle_key_stroke)]
        self.tree_wgd.add_complex_handlers(complex_handlers)

    def expand_namespaces(self, _input):
        """expand_namespaces

        :param _input:
        """
        tree_item = self.tree_wgd.values[self.tree_wgd.cursor_line]
        if tree_item.find_depth() == 1:
            ctx_name = get_context_name(tree_item.get_content().split(':')[0])
            try:
                if tree_item.expanded:
                    self.tree_wgd.h_collapse_tree('<')
                    for child in tree_item.get_children():
                        tree_item.remove_child(child)
                else:
                    ns_list = get_namespaces(ctx_name)
                    for ns in ns_list:
                        tree_item.new_child(content=ns, expanded=False)
                    self.tree_wgd.h_expand_tree('>')
            except:
                npyscreen.notify_confirm('Cluster is not reachable!', title='Warning')
        else:
            pass

    def set_context(self, _input):
        """Set context in kube config file

        :param _input:
        """
        tree_item = self.tree_wgd.values[self.tree_wgd.cursor_line]
        if tree_item.get_parent().get_content() == "Root":
            ctx_name = get_context_name(tree_item.get_content().split(':')[0])
            kube_config.use_context(name=ctx_name)
        else:
            namespace = self.tree_wgd.values[self.tree_wgd.cursor_line]
            context = get_context_name(namespace.get_parent().get_content().split(':')[0])
            kube_config.set_context(name=context,
                                    namespace=namespace.get_content())
            kube_config.use_context(name=context)
        exit()

    def delete_context():
        """Delete kubernetes context
        from kubeconfig file"""
        # backup_file_first
        pass

    @staticmethod
    def exit_func(_input):
        """Exit functon

        :param _input:
        """
        exit(0)


class K8sContextsApp(npyscreen.NPSAppManaged):
    """K8sContextsApp"""

    def onStart(self):
        """Add form on Start"""
        npyscreen.setTheme(npyscreen.Themes.ElegantTheme)
        self.addForm('MAIN', K8sContextsForm, name='K8S clusters/namespaces')


if __name__ == '__main__':
    kube_config = KubeConfig()
    try:
        contexts, active_context = config.list_kube_config_contexts()
    except FileNotFoundError:
        log.info('~/.kube/config file is missing or KUBECONFIG variable is not set!')
        exit()
    else:
        os.environ.setdefault('ESCDELAY', '25')
        os.environ.setdefault('LC_ALL', 'en_US.UTF-8')
        App = K8sContextsApp().run()

